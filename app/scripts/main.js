$(document).ready(function() {
	let $nav = $("._nav");
	let $doc = $("body");
	let $rates = $("._rates tbody");
	$("#login").click(function() {
		$("._modal").addClass("active");
	});
	$("#close").click(function() {
		$("._modal").removeClass("active");
	});
	$("#hamburger-icon").click(function() {
		$(this).toggleClass("open");
		$nav.toggleClass("open");
		$doc.toggleClass("fullscreen");
	});
	$(".tab-tab").click(function() {
		let tab = $(this).attr("content");
		$(".tab-tab").removeClass("active");
		$(this).addClass("active");
		$(".tab-content").each(function() {
			$(this).removeClass("active");
			if (tab == $(this).attr("index")) {
				$(this).addClass("active");
			}
		});
	});
	// lets populate the table
	$.ajax({
		url: "http://localhost:3000/rates",
		context: document.body
	}).done(function(response) {
		response.map(rate => {
			$rates.append(`
				<tr>
					<td>${rate.name}</td>
					<td>${rate.apy}%</td>
					<td>$${rate.earnings}</td>
				</tr>
			`);
		});
	});
});
